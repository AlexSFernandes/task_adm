# Projeto desenvolvido por Alexandre Salvador Fernandes

1. A modelagem do banco está disponível em: /webroot/img/modelagem_atividades.png
2. O script .sql para criar o banco está disponível em: /src/task_adm.sql

# CakePHP Application Skeleton

[![Build Status](https://img.shields.io/travis/cakephp/app/master.svg?style=flat-square)](https://travis-ci.org/cakephp/app)
[![License](https://img.shields.io/packagist/l/cakephp/app.svg?style=flat-square)](https://packagist.org/packages/cakephp/app)

A skeleton for creating applications with [CakePHP](https://cakephp.org) 3.x.

The framework source code can be found here: [cakephp/cakephp](https://github.com/cakephp/cakephp).

You can now either use your machine's webserver to view the default home page, or start
up the built-in webserver with:

```bash
bin/cake server -p 8765
```
Then visit `http://localhost:8765` to see the welcome page.



