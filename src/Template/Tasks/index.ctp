<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Task[]|\Cake\Collection\CollectionInterface $tasks
  */
?>

<div class="tasks index col-md-12 content">
    <h3><?= __('Registro de Atividades') ?></h3>
    <form method="get">
        <label>Filtrar por<label>
        <div class="row">
            <div class="col-md-2">
                <div class="form-group">
                    <label>Status</label>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Situação</label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <select name="status" class="form-control">
                    <option value="0" <?php if ($filter['status'] == 0) echo "selected"; ?>>Todos</option>
                    <?php foreach($all_status as $status) { ?>
                    <option value="<?php echo $status->id; ?>" <?php if ($filter['status'] == $status->id) echo "selected"; ?>><?php echo $status->name; ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-md-2">
                <select name="situation" class="form-control">
                    <option value="0" <?php if ($filter['situation'] == 0) echo "selected"; ?>>Todos</option>
                    <option value="1" <?php if ($filter['situation'] == 1) echo "selected"; ?>>Ativo</option>
                    <option value="2" <?php if ($filter['situation'] == 2) echo "selected"; ?>>Inativo</option>
                </select>
            </div>
            <div class="col-md-2">
                <button type="submit" class="btn btn-primary">Filtrar</button>
            </div>
        </div>

    </form>
    <table class="table" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col" width="4%">ID</th>
                <th scope="col" width="16%">Nome</th>
                <th scope="col" width="26%">Descrição</th>
                <th scope="col" width="9%">Início</th>
                <th scope="col" width="9%">Término</th>
                <th scope="col">Status</th>
                <th scope="col" width="7%">Situação</th>
                <th scope="col" class="actions"><?= __('Ações') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php if (count($tasks) == 0) { ?>
            <tr>
                <td colspan="8">Nenhuma atividade encontrada.</td>
            </tr>
            <?php } else { foreach ($tasks as $task): ?>
            <tr <?php if ($task->status_id == 4) echo 'style="background-color:#97d297;"'; ?>>
                <td><?= $this->Number->format($task->id) ?></td>
                <td><?= h($task->name) ?></td>
                <td><?= h($task->description) ?></td>
                <td><?= h(date('d/m/Y',strtotime($task->start_date))); ?></td>
                <td><?= h(empty($task->end_date)?'':date('d/m/Y',strtotime($task->end_date))); ?></td>
                <td><?= h($task->status_name) ?></td>
                <td><?= ($this->Number->format($task->situation) == 1)?'Ativo':'Inativo'; ?></td>
                <td class="actions">
                    <!--?= $this->Html->link(__('Ver'), ['action' => 'view', $task->id]) ?-->
                    <?php if ($task->status_id != 4) { ?>
                    <a href="/tasks/edit/<?php echo $task->id; ?>">Editar</a><br>
                    <?php } ?>
                    <a class="remove" data-id="<?= $this->Number->format($task->id) ?>" href="#">Remover</a>
                </td>
            </tr>
            <?php endforeach; } ?>
        </tbody>
    </table>

    <div class="row">
        <div class="col-md-2">
            <a href="/tasks/add" class="btn btn-primary">Nova Atividade</a>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function (){
    $('.remove').click(function(){
        var id = $(this).data('id');
        var line = $(this).parent().parent();
        console.log(line);
        if (confirm('Tem certeza que deseja remover a atividade #'+id+'?')) {
            $.ajax({
                    url: "/tasks/remove/"+id,
                    dataType: "json",
                    success: function( data ) {
                        console.log(data);
                        console.log(line);
                       line.remove();
                    }
            });
        }
        return false;
    });
});
</script>
