<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="tasks form col-md-12 content">
    <?php if (!empty($arr_err)) { ?>
    <div class="alert alert-danger" role="alert">
        <?php foreach ($arr_err as $message) {
            echo $message."<br>";
        } ?>
    </div>
    <?php } ?>
    <form method="post">
    <fieldset>
        <legend><?= __('Editar Atividade') ?></legend>
        <br>

        <div class="form-group">
            <label>Nome*</label>
            <input type="text" name="name" maxlength="255" class="form-control" value="<?php echo $task->name; ?>">
            <small id="fileHelp" class="form-text text-muted">Máx. 255 caracteres</small>
        </div>

        <div class="form-group">
            <label>Descrição*</label>
            <textarea name="description" rows="5" maxlength="600" class="form-control"><?php echo $task->description; ?></textarea>
            <small id="fileHelp" class="form-text text-muted">Máx. 600 caracteres</small>
        </div>

        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label>Data de Início*</label>
                    <input type="date" name="start_date" class="form-control" value="<?php echo date('Y-m-d',strtotime($task->start_date)); ?>"/>
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label>Data de fim</label>
                    <input type="date" name="end_date" class="form-control" value="<?php echo date('Y-m-d',strtotime($task->end_date)); ?>"/>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label>Status</label>
                    <select name="status_id" class="form-control">
                        <?php foreach ($all_status as $status) { ?>
                        <option value="<?php echo $status->id; ?>" <?php echo ($status->id == $task->status_id)? 'selected':''; ?>><?php echo $status->name; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label>Situação</label>
                    <select name="situation" class="form-control"> 
                        <option value="1" <?php echo ($status->situation == 1)? 'selected':''; ?>>Ativo</option>
                        <option value="2" <?php echo ($status->situation == 2)? 'selected':''; ?>>Inativo</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
        <small id="fileHelp" class="form-text text-muted">*campos obrigatórios</small>
        </div>
    </fieldset>
    <a href="/tasks" class="btn btn-secondary pull-left">Cancelar</a>
    <button type="submit" style="text-transform: none;" class="btn btn-primary">Salvar</button>
    </form>
</div>
