<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Controller\Component\FlashComponent;

/**
 * Tasks Controller
 *
 * @property \App\Model\Table\TasksTable $Tasks
 *
 * @method \App\Model\Entity\Task[] paginate($object = null, array $settings = [])
 */
class TasksController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public $all_status;

    public function initialize() {
        $status_model = TableRegistry::get('Status');
        $this->loadComponent('Flash');
        $this->all_status = $status_model->find()->all();

        $this->set('all_status',$this->all_status);
    }

    public function index()
    {
        $title = 'Atividades';
        $tasks_model = TableRegistry::get('tasks');

        $filter = array('situation' => 0, 'status' => 0);
        $data = $this->request->query;

        $filter['situation'] = (isset($data['situation']))? intval($data['situation']) : 0;
        $filter['status'] = (isset($data['status']))? intval($data['status']) : 0;

        $tasks = $tasks_model->getAllTasks($filter);

        $this->set('filter',$filter);
        $this->set('tasks',$tasks);
        $this->set('_serialize', ['tasks']);
        $this->set('title',$title);
    }
    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $title = 'Criar | Atividades';
        $status_model = TableRegistry::get('Status');
        $arr_err = array();
        $task = $this->Tasks->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->data();
            $arr_err = $this->validateForm($data);
            if (empty($arr_err)) {
                $task = $this->Tasks->patchEntity($task, $data);
                if ($this->Tasks->save($task)) {
                    $this->Flash->success(__('Atividade criada com sucesso!'));
                    return $this->redirect(['action' => 'index']);
                }
            }
            $this->set('post_data', $data);
        }
        $this->set('title',$title);
        $this->set('arr_err',$arr_err);
        $this->set(compact('task'));
        $this->set('_serialize', ['task']);
       
    }

    private function validateForm($task) {
        $empty_fields = array('name' => 'Nome','description' => 'Descrição','start_date' => 'Data de Início');
        $arr_err = array();
        foreach ($empty_fields as $name => $field) {
            if (!isset($task[$name]) || empty($task[$name])) {
                $arr_err[] = 'O campo '.$field.' não pode ser vazio';
            }
        }

        if (strlen($task['name']) > 255) {
            $arr_err[] = "O tamanho máximo do nome é de 255 caracteres";
        }

        if (strlen($task['description']) > 600) {
            $arr_err[] = "O tamanho máximo da descrição é de 600 caracteres";
        }

        if (!$this->validateTime($task['start_date'],$task['end_date'])) {
            $arr_err[] = 'A data de início deve ser anterior à data de término';
        }

        $task['status_id'] = intval($task['status_id']);
        $task['situation'] = intval($task['situation']);

        if ($task['status_id'] == 0) {
            $arr_err[] = 'Status inválido';
        } else if ($task['status_id'] == 4 && (!isset($task['end_date']) || empty($task['end_date']))) {
            $arr_err[] = 'Uma atividade com o status Concluído deve ter uma data de término';
        } else if ($task['situation'] == 0) {
            $arr_err[] = 'Situação inválida';
        }

        return $arr_err;
    }

    private function validateTime($start,$end) {
        if (!empty($end) && !empty($start) && strtotime($end) < strtotime($start)) {
            return false;
        }
        return true;
    }

    /**
     * Edit method
     *
     * @param string|null $id Task id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders index otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $task_model = TableRegistry::get('tasks');
        $arr_err = array();
        $task = $task_model->getTask($id);
        if (empty($task) || $task->status_id == 4) {
            $this->redirect('/tasks');
            return;
        }

        $title = $task->name.' | Atividades';

        if ($this->request->is('post')) {
            $data = $this->request->data();
            $arr_err = $this->validateForm($data);
            if (empty($arr_err)) {
                $result = $task_model->updateTasks($data);

                $task = $this->Tasks->patchEntity($task, $data);
                if ($this->Tasks->save($task)) {
                    $this->Flash->success(__('Atividade editada com sucesso!'));
                    return $this->redirect(['action' => 'index']);
                }
            }
        }
        $this->set('title',$title);
        $this->set('task', $task);
        $this->set('_serialize', ['task']);
        $this->set('arr_err',$arr_err);
    }

    public function remove($id = null)
    {
        $response = array('success' => false);

        $tasks_model = TableRegistry::get('tasks');
        $task = $tasks_model->getTask($id);

        $result = array();
        if (!empty($task)) {
            $result = $tasks_model->delete($task);
        }

        if (!empty($result)) {
            $response = array('success' => true);
        } 
        echo json_encode($response);
        die();

    }
}
