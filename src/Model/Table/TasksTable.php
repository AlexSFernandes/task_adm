<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Tasks Model
 *
 * @property |\Cake\ORM\Association\BelongsTo $Status
 *
 * @method \App\Model\Entity\Task get($primaryKey, $options = [])
 * @method \App\Model\Entity\Task newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Task[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Task|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Task patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Task[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Task findOrCreate($search, callable $callback = null, $options = [])
 */
class TasksTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('tasks');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('Status', [
            'foreignKey' => 'status_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('name')
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->scalar('description')
            ->requirePresence('description', 'create')
            ->notEmpty('description');

        $validator
            ->date('start_date')
            ->requirePresence('start_date', 'create')
            ->notEmpty('start_date');

        $validator
            ->date('end_date')
            ->allowEmpty('end_date');

        $validator
            ->integer('situation')
            ->allowEmpty('situation');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->existsIn(['status_id'], 'Status'));

        return $rules;
    }

    public function updateTasks($values) {
        
        $result = $this->query('CALL update_task ('.implode(',',$values).')')->execute();  
        return $result; 
    }

    //filter = array('situation' => INT, 'status' => 'INT');
    public function getAllTasks($filter) { 

        $tasks = $this->find()->join([
                'table' => 'status',
                'type' => 'LEFT',
                'conditions' => 'status_id = status.id' 
            ])->select(['id' => 'tasks.id', 'name' => 'tasks.name', 'description', 'start_date', 'end_date', 'situation', 'status_id' => 'status_id', 'status_name' => 'status.name']);

        if (empty($filter['status']) && !empty($filter['situation'])) {
            $tasks = $tasks->where(['situation' => $filter['situation']])->all();
        } else if (!empty($filter['status']) && empty($filter['situation'])) { 
            $tasks = $tasks->where(['status_id' => $filter['status']])->all();
        } else if (!empty($filter['status']) && !empty($filter['situation'])) {
            $tasks = $tasks->where(['status_id' => $filter['status'], 'situation' => $filter['situation']])->all();
        } else {
            $tasks = $tasks->where([])->all();
        }

        return $tasks;
    }

    public function getTask($id) {
        return $this->find()->where(['id' => $id])->first();
    }


}
